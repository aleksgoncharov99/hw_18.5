#include <iostream>


template <typename T>
class Stack
{
private:
    T *ptr; //stack pointer
    T head; //stack head
    int size = 0;
    

public:
    Stack(int size)
    {
        ptr = new T[size];
        head = 0;
    }
    void push(const T data)
    {
        //size++;
        //ptr = new T[size];
        ptr[head++] = data;
        std::cout << data << " pushed \n";
    }
     T pop()
    {
        std::cout << "Top removed \n";
        //ptr[head] = 0;
        return ptr[head--];
        
    }
    void output()
    {
        for (int i = head - 1; i >= 0; i--)
            std::cout << "|" << ptr[i] << std::endl;
    }
};




int main()
{
    Stack <int> S(6);
    S.push(10);
    S.push(20);
    S.push(30);
    S.push(40);
    S.push(50);
    S.push(60);
    S.output();
    S.pop();
    S.pop();
    S.output();

    return 0;
}
